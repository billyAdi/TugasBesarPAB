package com.example.user.tugasbesarpab;

/**
 * Created by WIN 10 on 5/10/2018.
 */

public class Lingkaran {
    public int rad;
    public double posX,posY,speedX,speedY;
    private boolean collided =false;

    public void collide(){
        this.collided=true;
    }

    public boolean getCollided(){
        return this.collided;
    }

    public double getPosX() {
        return posX;
    }

    public void setPosX(double posX) {
        this.posX = posX;
    }

    public double getPosY() {
        return posY;
    }

    public void setPosY(double posY) {
        this.posY = posY;
    }

    public int getRad() {
        return rad;
    }

    public void setRad(int rad) {
        this.rad = rad;
    }

    public double getSpeedX() {
        return speedX;
    }

    public void setSpeedX(double speedX) {
        this.speedX = speedX;
    }

    public double getSpeedY() {
        return speedY;
    }

    public int masa;

    public void setSpeedY(double speedY) {
        this.speedY = speedY;
    }
    

    public Lingkaran(int posX, int posY, int rad) {
        //this.masa = 10;
        this.posX = posX;
        this.posY = posY;
        this.rad = rad;
        this.speedX = 0;
        this.speedY = 0;
    }
}

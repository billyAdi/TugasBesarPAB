package com.example.user.tugasbesarpab;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by user on 5/1/2018.
 */

public class Presenter {
    private MainActivity ui;
    protected ArrayList<Integer> highScoreArrayList;
    private PenghitungScore penghitungScore;
    private SettingManager settingManager;
    private Setting setting;
    private Lingkaran goal;
    private int playercount=2;
    private int maxwidth,maxheight;
    private ArrayList<Lingkaran> obj;
    private ArrayList<Lingkaran> bonus;

    public Presenter(MainActivity ui) {
        this.ui = ui;
        this.highScoreArrayList=new ArrayList<Integer>();
        this.getHighScoreFromWebService(1);
        this.penghitungScore=new PenghitungScore();
        this.obj=new ArrayList<Lingkaran>();
        this.bonus=new ArrayList<Lingkaran>();
        this.settingManager =new SettingManager(this.ui);


    }

    public void renewArray(){

        this.obj=new ArrayList<Lingkaran>();
        this.bonus=new ArrayList<Lingkaran>();
    }

    public Lingkaran getPlayer(int i){
        return this.obj.get(i);
    }

    public Lingkaran getBonus(int i){
        return this.bonus.get(i);
    }
    public int getBonusCount(){
        return this.bonus.size();
    }
    public void removeBonus(ArrayList arr){
        for(int i =0;i<arr.size();i++) {
            this.bonus.remove((int)arr.get(i));
        }
    }

    public Lingkaran getEnd(){
        return this.goal;
        //return this.obj.get(1);
    }

    public int getPlayerSize(){
        return this.obj.size();
    }

    public boolean isCollide(){
        for(int i =0;i<obj.size();i++){
            if(!obj.get(i).getCollided()){
                return false;
            }
        }
        return true;
    }

    public void gerakPlayer(int x,int y,int canvasWidth,int canvasHeight){
        for(int i = 0;i<obj.size();i++) {
            Lingkaran player = this.obj.get(i);
            if (!player.getCollided()) {
                player.setSpeedX(player.getSpeedX() + x);
                player.setSpeedY(player.getSpeedY() - y);

                double temp = player.getPosX() + player.getSpeedX();
                if (temp < player.getRad()) {
                    player.setSpeedX(player.getSpeedX()*-0.5);
                    temp = player.getRad();
                } else if (temp >= (canvasWidth - player.getRad())) {
                    player.setSpeedX(player.getSpeedX()*-0.5);
                    temp = canvasWidth - player.getRad();
                }

                player.setPosX((int)temp);

                temp = player.getPosY() + player.getSpeedY();
                if (temp < player.getRad()) {
                    player.setSpeedY(player.getSpeedY()/2);
                    temp = player.getRad();
                } else if (temp >= (canvasHeight - player.getRad())) {
                    player.setSpeedY(-1*(player.getSpeedY()/2));
                    temp = canvasHeight - player.getRad();
                }

                player.setPosY((int)temp);
            }
        }


    }

    public void addBonus(int posx,int posy,int radius){
        this.bonus.add(new Lingkaran(posx,posy,radius));
    }

    public void addObj(int posx,int posy,int radius){
        this.obj.add(new Lingkaran(posx,posy,radius));
    }

    public void setPlayer(int idx,int posx,int posy){
        this.obj.get(idx).setPosX(posx);
        this.obj.get(idx).setPosX(posy);
    }

    public void ubahGoal(int posx,int posy,int radius){
        this.goal = new Lingkaran(posx,posy,radius);
    }

    //yg disimpen tuh index spinner nya
    public void saveSettings(int speed,int color1, int color2,int bonus){
        this.settingManager.saveSettings(speed,color1,color2,bonus);
    }

    //load index nya ke settings fragment
    public int[] loadSettings(){
        return this.settingManager.loadSettings();
    }



    public ArrayList<Integer> getHighScoreList(){
        return this.highScoreArrayList;
    }

    public void updateHighScoreArray(){


        Collections.sort(this.highScoreArrayList, Collections.reverseOrder());
        int size= this.highScoreArrayList.size();
        this.highScoreArrayList.remove(size-1);

        this.ui.updateListView();


         this.updateHighScoreToWebService();

    }

    public Setting getSetting(){
        Setting setting=new Setting(this.ui.settingsFragment.getSpinnerSpeedValue(),this.ui.settingsFragment.getSpinnerColor1Value(),this.ui.settingsFragment.getSpinnerColor2Value(),this.ui.settingsFragment.getSpinnerBonusValue());
        return setting;
    }

    public void addNewScore(int score){

        this.highScoreArrayList.add(score);
        this.updateHighScoreArray();
    }

    public void gerakPlayer(int idx){

            obj.get(idx).setPosX((obj.get(idx).getSpeedX()+obj.get(idx).getPosX()));
            obj.get(idx).setPosY((obj.get(idx).getSpeedY()+obj.get(idx).getPosY()));

    }

    public void setEdge(int width,int height){
        this.maxwidth=width;
        this.maxheight=height;
    }

    public void gerakPlayer(){


        Lingkaran player1= getPlayer(0);
        Lingkaran player2= getPlayer(1);
        //if(player1.getCollided() || player2.getCollided()) {
            //beda posisi
            double bx = player2.getPosX() - player1.getPosX();
            double by = player2.getPosY() - player1.getPosY();
            // trigono
            double vnorm = Math.hypot(bx, by);
            bx = bx / vnorm;
            by = by / vnorm;

            //massa bola
            //player1.getMassa();
            //player2.getMassa();
            double m1 = 10;
            double m2 = 8;
            double mt = m1 + m2;
            //kecepatan dan arah
            double v1 = player1.getSpeedX() * bx + player1.getSpeedY() * by;
            double v2 =  player2.getSpeedX() * bx + player2.getSpeedY() * by;

            //penentuan arah pantul
            double qx = by;
            double qy = bx *-1;
            double u1 = player1.getSpeedX() * qx + player1.getSpeedY() * qy;
            double u2 = player2.getSpeedY() * qx + player2.getSpeedY() * qy;
            //if(u1)>u2 && (u1!=0 && u2 !=0){
            // ubah kec+arah gerak
            double v1f = ((2.0) * m2 * v2 + v1 * (m1 -  m2)) / mt;
            double v2f = ((2.0) * m1 * v1 + v2 * (m2 -  m1)) / mt;


           //if((v2f * px + u2 * qx)!=0){
               player2.setSpeedX((v2f * bx + u2 * qx));
          // }
           //if((v1f * px + u1 * qx )!=0){
               player1.setSpeedX((v1f * bx + u1 * qx));

           //}

        //if((v2f * py + u2 * qy) !=0){
            player2.setSpeedY( (v2f * by + u2 * qy));
        //}
        //if((v1f * py + u1 * qy)!=0){
            player1.setSpeedY((v1f * by + u1 * qy));
        /*
        else {
            player1.setSpeedX((int) (v1f * px + u1 * qx));
            player1.setSpeedY((int) (v1f * py + u1 * qy));

            player2.setSpeedX((int) (v2f * px + u2 * qx));
            player2.setSpeedY((int) (v2f * py + u2 * qy));
        }*/
        gerakPlayer(0);

        gerakPlayer(1);

            //pos x
            if(player1.getPosX()- player1.getRad()<=0 && cekCollide(player1,player2)){
                player1.setPosX(player1.getRad());
                player2.setPosX(player1.getPosX()+player1.getRad()+player2.getRad());
                if(player1.getSpeedX()<0 && player2.getSpeedX()<0){
                    player2.setSpeedX(player2.getSpeedX()*-1);
                }
            }else if(player2.getPosX()-player2.getRad()<=0 && cekCollide(player1,player2)){
                player2.setPosX(player2.getRad());
                player1.setPosX(player2.getPosX()+player2.getRad()+player1.getRad());
                if(player1.getSpeedX()<0 && player2.getSpeedX()<0){
                    player1.setSpeedX(player1.getSpeedX()*-1);
                }
            }else if(player1.getPosX()+player1.getRad()>=maxwidth && cekCollide(player1,player2)){
                player1.setPosX(maxwidth-player1.getRad());
                player2.setPosX((player1.getPosX()-player1.getRad())-player2.getRad());
                if(player1.getSpeedX()>0 && player2.getSpeedX()>0){
                    player1.setSpeedX(player1.getSpeedX()*-1);
                }
            }else if(player2.getPosX()+player2.getRad()>=maxwidth && cekCollide(player1,player2)){
                player2.setPosX(maxwidth-player2.getRad());
                player1.setPosX((player2.getPosX()-player2.getRad())-player1.getRad());
                if(player1.getSpeedX()>0 && player2.getSpeedX()>0){
                    player2.setSpeedX(player2.getSpeedX()*-1);
                }
            }

            /*if(player1.getPosX()>0 && cekCollide(player1,player2)){
                player2.setPosX(player1.getRad()*2);
            }else if(player2.getPosX()>0 && cekCollide(player1,player2)){
                player1.setPosX(player2.getRad()*2);
            }*/
            //pos y
            if(player1.getPosY()- player1.getRad()<=0 && cekCollide(player1,player2)){
                player1.setPosY(player1.getRad());
                player2.setPosY(player1.getPosY()+player1.getRad()+player2.getRad());
                if(player1.getSpeedY()<0 && player2.getSpeedY()<0){
                    player2.setSpeedY(player2.getSpeedX()*-1);
                }
            }else if(player2.getPosY()-player2.getRad()<=0 && cekCollide(player1,player2)){
                player2.setPosY(player2.getRad());
                player1.setPosY(player2.getPosY()+player2.getRad()+player1.getRad());
                if(player1.getSpeedY()<0 && player2.getSpeedY()<0){
                    player1.setSpeedY(player1.getSpeedY()*-1);
                }
            }else if(player1.getPosY()+player1.getRad()>=maxheight && cekCollide(player1,player2)){
                player1.setPosY(maxheight-player1.getRad());
                player2.setPosY((player1.getPosY()-player1.getRad())-player2.getRad());
                if(player1.getSpeedX()>0 && player2.getSpeedX()>0){
                    player2.setSpeedX(player2.getSpeedX()*-1);
                }
            }else if(player2.getPosY()+player2.getRad()>=maxheight && cekCollide(player1,player2)){
                player2.setPosY(maxheight-player2.getRad());
                player1.setPosY((player2.getPosY()-player2.getRad())-player1.getRad());
                if(player1.getSpeedX()>0 && player2.getSpeedX()>0){
                    player1.setSpeedX(player1.getSpeedX()*-1);
                }
            }
        /* cek collide
            if(cekCollide(player1,player2)){
                double bedaposx=player1.getPosX()-player2.getPosX();
                double bedaposy=player1.getPosY()-player2.getPosY();
                if(player1.getPosX()>player2.getPosX()){
                    player1.setPosX(player1.getPosX()-(player1.getRad()-bedaposx));
                    player2.setPosX(player2.getPosX()+(player2.getRad()-bedaposx));
                }
                else{
                    player1.setPosX(player1.getPosX()+(player1.getRad()-bedaposx));
                    player2.setPosX(player2.getPosX()-(player2.getRad()-bedaposx));
                }
                if(player1.getPosY()>player2.getPosY()){
                    player1.setPosY(player1.getPosY()-(player1.getRad()-bedaposy));
                    player2.setPosY(player2.getPosY()+(player2.getRad()-bedaposy));
                }
                else{
                    player1.setPosY(player1.getPosY()+(player1.getRad()-bedaposy));
                    player2.setPosY(player2.getPosY()-(player2.getRad()-bedaposy));
                }
            }*/
            //gerakPlayer(0);

            //gerakPlayer(1);



        //}
        /*if(player1.getCollided() || player2.getCollided()){
            player2.setSpeedX(operatorx * player2.getSpeedX());
            player2.setSpeedY(operatory * player2.getSpeedY());
            gerakPlayer(1);
        }
        else if(player2.getCollided()){
            player1.setSpeedX(operatorx * player1.getSpeedX());
            player1.setSpeedY(operatory * player1.getSpeedY());
            gerakPlayer(0);
        }
        else {

            int speedx1 = player1.getSpeedX();
            int speedy1 = player1.getSpeedY();
            int speedx2 = player2.getSpeedX();
            int speedy2 = player2.getSpeedY();

            if ((speedx1 >= 0 && speedx2 >= 0) || (speedx2 <= 0 && speedx1 <= 0)) {
                x1 = true;
            }

            if ((speedy1 >= 0 && speedy2 >= 0) || (speedy2 <= 0 && speedy1 <= 0)) {
                y1 = true;
            }


            gerakPlayer(0, x1, y1);
            gerakPlayer(1, x1, y1);
        }*/
    }

    public void gerakPlayer(int idx,boolean x,boolean y){

        if(x) {
            obj.get(idx).setPosX(obj.get(idx).getSpeedX() + obj.get(idx).getPosX());
        }
        if(y) {
            obj.get(idx).setPosY(obj.get(idx).getSpeedY() + obj.get(idx).getPosY());
        }

    }


    public boolean cekCollide(Lingkaran l1,Lingkaran l2){
        double xDif = l1.getPosX() - l2.getPosX();
        double yDif = l1.getPosY() - l2.getPosY();
        double distanceSquared = xDif * xDif + yDif * yDif;
        return distanceSquared < (l1.getRad() + l2.getRad()) * (l1.getRad() + l2.getRad());
    }

    public float hitungArah(float val){
        return Math.signum(val) * Math.abs(val);
    }

    public void getHighScoreFromWebService(final int page){
        if(page<8){
            RequestQueue queue = Volley.newRequestQueue(this.ui);
            String url ="http://pab.labftis.net/api.php?api_key=2015730053&page="+page;

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {

                                JSONObject json=new JSONObject(response);
                                JSONArray data=json.getJSONArray("data");

                                for (int i=0;i<data.length();i++){
                                    JSONObject object=data.getJSONObject(i);
                                    int score=object.getInt("value");
                                    highScoreArrayList.add(score);
                                }

                                getHighScoreFromWebService(page+1);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            queue.add(stringRequest);
        }
        else if(page==8){
              this.ui.updateListView();
        }
    }

    public void updateHighScoreToWebService(){
        for(int i=0;i<this.highScoreArrayList.size();i++){
            RequestQueue queue = Volley.newRequestQueue(this.ui);
            String url ="http://pab.labftis.net/api.php";
            final int index=i;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            })
            {
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("api_key","2015730053");
                    params.put("order",index+1+"");
                    params.put("value", highScoreArrayList.get(index)+"");

                    return params;
                }

            };

            queue.add(stringRequest);
        }

    }

    public int getScore(int count,int bonusCounter){
        return this.penghitungScore.getScore(count,bonusCounter);
    }


}
